package com.amaurote.bazar.repository;

import com.amaurote.bazar.domain.Notification;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NotificationRepository extends JpaRepository<Notification, Long> {
}
