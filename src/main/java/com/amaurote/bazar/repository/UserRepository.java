package com.amaurote.bazar.repository;

import com.amaurote.bazar.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, String> {

    User findByEmail(String email);

    Iterable<User> findByNameLike(String name);

    void removeByEmail(String email);
}
