package com.amaurote.bazar.repository;

import com.amaurote.bazar.domain.AdvType;
import com.amaurote.bazar.domain.Advertisement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AdvertisementRepository extends JpaRepository<Advertisement, Long> {

    Iterable<Advertisement> findAllByTypeOrderByDateCreated(AdvType type);

    Iterable<Advertisement> findByTitleLikeOrderByDateCreated(String title);
}
