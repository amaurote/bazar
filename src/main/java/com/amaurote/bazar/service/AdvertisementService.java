package com.amaurote.bazar.service;

import com.amaurote.bazar.domain.AdvType;
import com.amaurote.bazar.domain.Advertisement;
import com.amaurote.bazar.domain.Notification;
import com.amaurote.bazar.domain.User;
import com.amaurote.bazar.domain.UserRole;
import com.amaurote.bazar.dto.AdvertisementDTO;
import com.amaurote.bazar.dto.DTOFactory;
import com.amaurote.bazar.dto.ResponseDTO;
import com.amaurote.bazar.repository.AdvertisementRepository;
import com.amaurote.bazar.repository.NotificationRepository;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashSet;

@Service
public class AdvertisementService {

    private final AdvertisementRepository advertisementRepository;
    private final NotificationRepository notificationRepository;
    private final UserService userService;
    private final DTOFactory dtoFactory;

    public AdvertisementService(AdvertisementRepository advertisementRepository, NotificationRepository notificationRepository, UserService userService, DTOFactory dtoFactory) {
        this.advertisementRepository = advertisementRepository;
        this.notificationRepository = notificationRepository;
        this.userService = userService;
        this.dtoFactory = dtoFactory;
    }

    public AdvertisementDTO getAdvertisementsById(Long id) {
        return dtoFactory.createAdvertisementDTO(advertisementRepository.findById(id).orElse(null));
    }

    public HashSet<AdvertisementDTO> getAllAdvertisementsByType(AdvType type) {
        return dtoFactory.createAllAdvertisementDTOs(advertisementRepository.findAllByTypeOrderByDateCreated(type));
    }

    public HashSet<AdvertisementDTO> getAllAdvertisements() {
        return dtoFactory.createAllAdvertisementDTOs(advertisementRepository.findAll());
    }

    public HashSet<AdvertisementDTO> getByTitle(String title) {
        return dtoFactory.createAllAdvertisementDTOs(advertisementRepository.findByTitleLikeOrderByDateCreated("%" + title + "%"));
    }

    public boolean createNewAdvertisement(AdvertisementDTO dto, String email) {
        Advertisement advertisement = new Advertisement();

        advertisement.setAuthor(userService.findUserByEmail(email));
        advertisement.setTitle(dto.getTitle());
        advertisement.setDescription(dto.getDescription());
        advertisement.setType(AdvType.valueOf(dto.getType().toUpperCase()));
        advertisement.setCost(dto.getCost());
        advertisement.setDateCreated(new Date());

        advertisementRepository.save(advertisement);
        return true;
    }

    public boolean deleteAdvertisementById(Long id) {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username;
        if (principal instanceof UserDetails)
            username = ((UserDetails) principal).getUsername();
        else
            username = principal.toString();

        User user = userService.findUserByEmail(username);
        Advertisement advertisement = advertisementRepository.findById(id).orElse(null);
        User author = advertisement.getAuthor();

        if (user.getUserRole() == UserRole.ADMIN || author.equals(user))
            advertisementRepository.deleteById(id);

        return true;
    }

    public void sendResponseNotification(ResponseDTO dto) {
        Notification notification = new Notification();

        notification.setEmail(dto.getRespondentEmail());
        notification.setDateCreated(new Date());
        notification.setMessage(dto.getRespondentEmail() + ": " + dto.getResponse());

        notificationRepository.save(notification);
    }

}
