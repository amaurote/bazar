package com.amaurote.bazar.service;

import com.amaurote.bazar.domain.Notification;
import com.amaurote.bazar.domain.User;
import com.amaurote.bazar.domain.UserRole;
import com.amaurote.bazar.dto.DTOFactory;
import com.amaurote.bazar.dto.UserDTO;
import com.amaurote.bazar.repository.NotificationRepository;
import com.amaurote.bazar.repository.UserRepository;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.transaction.Transactional;
import java.util.Date;
import java.util.Formatter;
import java.util.HashSet;
import java.util.Random;

@Service
public class UserService {

    private static final String CHAR_SET = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    private final Random random;

    private final UserRepository userRepository;
    private final NotificationRepository notificationRepository;
    private final DTOFactory dtoFactory;

    public UserService(UserRepository userRepository, NotificationRepository notificationRepository, DTOFactory dtoFactory) {
        this.userRepository = userRepository;
        this.notificationRepository = notificationRepository;
        this.dtoFactory = dtoFactory;

        this.random = new Random();
    }

    public HashSet<UserDTO> getAllUsers() {
        return dtoFactory.createAllUserDTOs(userRepository.findAll());
    }

    public HashSet<UserDTO> getByName(String name) {
        return dtoFactory.createAllUserDTOs(userRepository.findByNameLike("%" + name + "%"));
    }

    @Transactional
    public boolean createUser(UserDTO userDTO) {
        // create new User
        User user = new User();

        try {
            InternetAddress email = new InternetAddress(userDTO.getEmail());
            email.validate();
            user.setEmail(email.toString());

            user.setName(userDTO.getName());
            user.setDateCreated(new Date());

            user.setUserRole(UserRole.valueOf(userDTO.getRole().toUpperCase()));
            user.setAdvertisements(new HashSet<>());
        } catch (IllegalArgumentException | AddressException e) {
            // TODO
            return false;
        }

        // generate random password
        StringBuilder newPassword = new StringBuilder(6);

        for (int i = 0; i < 6; i++)
            newPassword.append(CHAR_SET.charAt(random.nextInt(CHAR_SET.length())));

        // encrypt password and save the user
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        user.setPassword(encoder.encode(newPassword.toString()));
        user.setTemporalPassword(true);
        userRepository.save(user);

        // create new Notification
        Notification notification = new Notification();
        Date dateCreated = new Date();
        Formatter message = new Formatter();

        notification.setEmail(user.getEmail());
        notification.setDateCreated(dateCreated);
        notification.setDateExpiration(DateUtils.addHours(dateCreated, 48));

        message.format(
                "Dear %s. Your account in GL Bazar was created with temporal password: %s\n " +
                        "Please change your password within 48 hours.",
                user.getName(), newPassword.toString());
        notification.setMessage(message.toString());
        message.close();

        notificationRepository.save(notification);

        return true;
    }

    public void deleteUserById(String id) {
        userRepository.deleteById(id);
    }

    @Transactional
    public void deleteUserByEmail(String email) {
        userRepository.removeByEmail(email);
    }

    public boolean isUserPresent(String email) {
        return (userRepository.findByEmail(email) != null);
    }

    public User findUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }

}
