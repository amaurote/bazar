package com.amaurote.bazar.controller;

import com.amaurote.bazar.dto.AdvertisementDTO;
import com.amaurote.bazar.dto.ResponseDTO;
import com.amaurote.bazar.service.AdvertisementService;
import com.amaurote.bazar.service.UserService;
import org.springframework.context.annotation.Profile;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;

@Profile("thyme")
@RequestMapping
@Controller
public class AdvertisementControllerThyme {

    private final AdvertisementService advertisementService;
    private final UserService userService;

    public AdvertisementControllerThyme(AdvertisementService advertisementService, UserService userService) {
        this.advertisementService = advertisementService;
        this.userService = userService;
    }

    @GetMapping("/advertisements")
    public String listAdvertisements(Model model, @RequestParam(defaultValue = "") String title, @RequestParam(required = false) String id) {
        if (id != null && !id.equals("")) {
            Long numericId = 0L;
            try {
                numericId = Long.valueOf(id);
            } catch (Exception e) {
            }

            if (numericId != 0) {
                model.addAttribute("responseDTO", new ResponseDTO());
                model.addAttribute("advertisementDTO", advertisementService.getAdvertisementsById(numericId));
                return "views/advertisement";
            }
        }

        model.addAttribute("advertisements", advertisementService.getByTitle(title));
        return "views/adverlist";
    }

    @DeleteMapping("/advertisements")
    public String deleteByEmail(Model model, AdvertisementDTO advertisementDTO) {
        advertisementService.deleteAdvertisementById(advertisementDTO.getId());
        model.addAttribute("advertisements", advertisementService.getAllAdvertisements());
        return "views/adverlist";
    }

    @GetMapping("/newadv")
    public String newAdvertisementForm(Model model) {
        model.addAttribute("advertisementDTO", new AdvertisementDTO());
        return "views/newAdForm";
    }

    @PostMapping("/newadv")
    public String newAdvertisement(@Valid AdvertisementDTO dto, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "views/newAdFrom";
        }

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username;
        if (principal instanceof UserDetails)
            username = ((UserDetails) principal).getUsername();
        else
            username = principal.toString();

        advertisementService.createNewAdvertisement(dto, username);

        return "redirect:/advertisements";
    }

    @PostMapping("/response")
    public String respond(@Valid ResponseDTO dto, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            return "views/advertisement";
        }

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username;
        if (principal instanceof UserDetails)
            username = ((UserDetails) principal).getUsername();
        else
            username = principal.toString();

        dto.setRespondentEmail(username);
        advertisementService.sendResponseNotification(dto);

        return "views/response";
    }
}
