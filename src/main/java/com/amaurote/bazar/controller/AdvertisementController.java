package com.amaurote.bazar.controller;

import com.amaurote.bazar.domain.AdvType;
import com.amaurote.bazar.dto.AdvertisementDTO;
import com.amaurote.bazar.service.AdvertisementService;
import com.amaurote.bazar.view.JSONViews;
import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;

@Profile("default")
@RestController
@RequestMapping("/advert")
public class AdvertisementController {

    private final AdvertisementService advertisementService;

    public AdvertisementController(AdvertisementService advertisementService) {
        this.advertisementService = advertisementService;
    }

    @JsonView(JSONViews.Simple.class)
    @GetMapping("/")
    @ResponseBody
    public Iterable<AdvertisementDTO> getAllAdvertisements(@RequestParam(value = "type", required = false) String type,
                                                           @RequestParam(value = "id", required = false) Long id) {
        if (id != null && !id.equals("")) {
            return Arrays.asList(advertisementService.getAdvertisementsById(id));
        }

        if (type != null && !type.equals("")) {
            try {
                return advertisementService.getAllAdvertisementsByType(AdvType.valueOf(type.toUpperCase()));
            } catch (IllegalArgumentException e) {
                return null;
            }
        }

        return advertisementService.getAllAdvertisements();
    }
}
