package com.amaurote.bazar.controller;

import com.amaurote.bazar.dto.UserDTO;
import com.amaurote.bazar.service.UserService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;

@Profile("thyme")
@RequestMapping("/users")
@Controller
public class UserControllerThyme {

    private final UserService userService;

    public UserControllerThyme(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public String listUsers(Model model, @RequestParam(defaultValue = "") String name) {
        model.addAttribute("users", userService.getByName(name));
        return "views/userlist";
    }

    @DeleteMapping
    public String deleteByEmail(Model model, UserDTO userDTO, HttpSession session) {
        userService.deleteUserByEmail(userDTO.getEmail());
        model.addAttribute("users", userService.getAllUsers());
        return "views/userlist";
    }

}
