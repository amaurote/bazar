package com.amaurote.bazar.controller;

import com.amaurote.bazar.dto.UserDTO;
import com.amaurote.bazar.service.UserService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@Profile("thyme")
@RequestMapping("/register")
@Controller
public class RegisterControllerThyme {

    private final UserService userService;

    public RegisterControllerThyme(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public String registerForm(Model model) {
        model.addAttribute("userDTO", new UserDTO());
        return "views/registrationForm";
    }

    @PostMapping
    public String registerUser(@Valid UserDTO userDTO, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            return "views/registrationForm";
        }

        if (userService.isUserPresent(userDTO.getEmail())) {
            model.addAttribute("exist", true);
            return "views/registrationForm";
        }

        return (userService.createUser(userDTO)) ? "views/success" : "views/failure";
    }

}
