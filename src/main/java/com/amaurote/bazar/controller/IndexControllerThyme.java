package com.amaurote.bazar.controller;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Profile("thyme")
@Controller
public class IndexControllerThyme {

    @GetMapping(value = {"", "/"})
    public String showIndexPage() {
        return "index";
    }

    @GetMapping("/login")
    public String showLoginForm() {
        return "views/loginForm";
    }

}
