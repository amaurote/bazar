package com.amaurote.bazar.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.util.Date;

public class UserDTO {

    @NotEmpty(message = "This field should not be empty")
    @Email(message = "Please add valid email address")
    private String email;

    @NotEmpty(message = "This field should not be empty")
    private String name;

    @NotEmpty(message = "This field should not be empty")
    private String role;

    private Date dateCreated;

    public UserDTO() {
    }

    public UserDTO(String email, String name, String role, Date dateCreated) {
        this.email = email;
        this.name = name;
        this.role = role;
        this.dateCreated = dateCreated;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }
}
