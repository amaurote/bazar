package com.amaurote.bazar.dto;

import javax.validation.constraints.NotEmpty;

public class ResponseDTO {


    private Long advertisementId;
    private String respondentEmail;

    @NotEmpty(message = "This field should not be empty")
    private String response;

    public ResponseDTO() {
    }

    public ResponseDTO(Long advertisementId, String respondentEmail, String response) {
        this.advertisementId = advertisementId;
        this.respondentEmail = respondentEmail;
        this.response = response;
    }

    public Long getAdvertisementId() {
        return advertisementId;
    }

    public void setAdvertisementId(Long advertisementId) {
        this.advertisementId = advertisementId;
    }

    public String getRespondentEmail() {
        return respondentEmail;
    }

    public void setRespondentEmail(String respondentEmail) {
        this.respondentEmail = respondentEmail;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}
