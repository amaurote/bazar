package com.amaurote.bazar.dto;

import javax.validation.constraints.NotEmpty;
import java.util.Date;

public class AdvertisementDTO {

    // TODO custom invalid messages in properties

    private Long id;
    private String author;

    @NotEmpty(message = "This field should not be empty")
    private String title;

    @NotEmpty(message = "This field should not be empty")
    private String description;

    @NotEmpty(message = "This field should not be empty")
    private String type;

    @NotEmpty(message = "This field should not be empty")
    private String cost;

    private Date dateCreated;

    public AdvertisementDTO() {
    }

    public AdvertisementDTO(Long id, String author, String title, String description, String type, String cost, Date dateCreated) {
        this.id = id;
        this.author = author;
        this.title = title;
        this.description = description;
        this.type = type;
        this.cost = cost;
        this.dateCreated = dateCreated;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }
}
