package com.amaurote.bazar.dto;

import com.amaurote.bazar.domain.Advertisement;
import com.amaurote.bazar.domain.User;
import org.springframework.stereotype.Component;

import java.util.HashSet;

@Component
public class DTOFactory {

    public UserDTO createUserDTO(User user) {
        if (user == null)
            return null;

        UserDTO userDTO = new UserDTO();

        userDTO.setEmail(user.getEmail());
        userDTO.setName(user.getName());
        userDTO.setRole(user.getUserRole().name());
        userDTO.setDateCreated(user.getDateCreated());

        return userDTO;
    }

    public HashSet<UserDTO> createAllUserDTOs(Iterable<User> users) {
        HashSet<UserDTO> dtos = new HashSet<>();
        for (User user : users) {
            dtos.add(createUserDTO(user));
        }

        return dtos;
    }

    public AdvertisementDTO createAdvertisementDTO(Advertisement advertisement) {
        if (advertisement == null)
            return null;

        AdvertisementDTO advDTO = new AdvertisementDTO();

        advDTO.setId(advertisement.getId());
        advDTO.setAuthor(advertisement.getAuthor().getName());
        advDTO.setTitle(advertisement.getTitle());
        advDTO.setDescription(advertisement.getDescription());
        advDTO.setType(advertisement.getType().name());
        advDTO.setCost(advertisement.getCost());
        advDTO.setDateCreated(advertisement.getDateCreated());

        return advDTO;
    }

    public HashSet<AdvertisementDTO> createAllAdvertisementDTOs(Iterable<Advertisement> advertisements) {
        HashSet<AdvertisementDTO> dtos = new HashSet<>();
        for (Advertisement adv : advertisements) {
            dtos.add(createAdvertisementDTO(adv));
        }

        return dtos;
    }

}
