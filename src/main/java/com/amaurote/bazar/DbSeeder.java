package com.amaurote.bazar;

import com.amaurote.bazar.domain.AdvType;
import com.amaurote.bazar.domain.Advertisement;
import com.amaurote.bazar.domain.User;
import com.amaurote.bazar.domain.UserRole;
import com.amaurote.bazar.repository.AdvertisementRepository;
import com.amaurote.bazar.repository.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;

@Component
public class DbSeeder implements CommandLineRunner {

    private final UserRepository userRepository;
    private final AdvertisementRepository advertisementRepository;

    public DbSeeder(UserRepository userRepository, AdvertisementRepository advertisementRepository) {
        this.userRepository = userRepository;
        this.advertisementRepository = advertisementRepository;
    }

    @Override
    public void run(String... args) {

        // users URg1oC
        User adminUser = new User("admin@admin.com", "$2a$10$wAGMl8F5tf8qZLo3X7/Iz.B7jRiFZFeTc0YLNlEIb8bEUAmcmCevW", false, "Admin User", new Date(), UserRole.ADMIN, new HashSet<>());
        User regularUser1 = new User("regular1@user.com", "$2a$10$wAGMl8F5tf8qZLo3X7/Iz.B7jRiFZFeTc0YLNlEIb8bEUAmcmCevW", false, "Regular User One", new Date(), UserRole.USER, new HashSet<>());
        User regularUser2 = new User("regular2@user.com", "pass", false, "Regular User Two", new Date(), UserRole.USER, new HashSet<>());
        User regularUser3 = new User("regular3@user.com", "pass", false, "Regular User Three", new Date(), UserRole.USER, new HashSet<>());

        userRepository.saveAll(Arrays.asList(adminUser, regularUser1, regularUser2, regularUser3));

        // adverts
        Advertisement advert1 = new Advertisement(regularUser1, "Plumbus for sale!", "Regular old plumbus. I got several for spare.",
                AdvType.SELL, "150 Schmeckles", new Date());

        Advertisement advert2 = new Advertisement(regularUser2, "Hiring waitresses to Lil' Bits",
                "Hey listen, is your mouth tiny and small and you are looking for a job? Then why don't you come work to our Little bits. Where the food is tiny. It looks like regular food but really tiny.",
                AdvType.BUY, "500 Brapples p.m.", new Date());

        Advertisement advert3 = new Advertisement(regularUser3, "Wanna sell a bicycle.", "Just a regular item to sell... A bicycle made in 1972.",
                AdvType.SELL, "70 EUR", new Date());

        Advertisement advert4 = new Advertisement(regularUser3, "Searching for a flat close to town center.", "I'm pretty foldable, 1x1 room is right enough for me.",
                AdvType.BUY, "for free", new Date());

        advertisementRepository.saveAll(Arrays.asList(advert1, advert2, advert3, advert4));
    }


}
