package com.amaurote.bazar.domain;

public enum UserRole {
    ADMIN, USER
}
