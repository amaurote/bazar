package com.amaurote.bazar.domain;

import com.amaurote.bazar.view.JSONViews;
import com.fasterxml.jackson.annotation.JsonView;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.util.Date;

@Entity
public class Advertisement {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonView(JSONViews.Simple.class)
    @Column(name = "ID", updatable = false, nullable = false)
    private Long id;

    @JsonView(JSONViews.Simple.class)
    @ManyToOne(fetch = FetchType.LAZY)
    private User author;

    @JsonView(JSONViews.Simple.class)
    @Column(name = "TITLE", nullable = false)
    private String title;

    @JsonView(JSONViews.Simple.class)
    @Column(name = "DESCRIPTION", columnDefinition = "TEXT")
    private String description;

    @Enumerated(value = EnumType.STRING)
    @JsonView(JSONViews.Simple.class)
    @Column(name = "TYPE", nullable = false)
    private AdvType type;

    @JsonView(JSONViews.Simple.class)
    @Column(name = "COST")
    private String cost;

    @JsonView(JSONViews.Simple.class)
    @Column(name = "DATE_CREATED", updatable = false, nullable = false)
    private Date dateCreated;

    public Advertisement() {
    }

    public Advertisement(User author, String title, String description, AdvType type, String cost, Date dateCreated) {
        this.author = author;
        this.title = title;
        this.description = description;
        this.type = type;
        this.cost = cost;
        this.dateCreated = dateCreated;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public AdvType getType() {
        return type;
    }

    public void setType(AdvType type) {
        this.type = type;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }
}
