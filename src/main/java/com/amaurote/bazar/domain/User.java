package com.amaurote.bazar.domain;

import com.amaurote.bazar.view.JSONViews;
import com.fasterxml.jackson.annotation.JsonView;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.Parameter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.Date;
import java.util.Set;

@Entity
public class User {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator",
            parameters = {
                    @Parameter(
                            name = "uuid_gen_strategy_class",
                            value = "org.hibernate.id.uuid.CustomVersionOneStrategy"
                    )
            }
    )
    @JsonView(JSONViews.Detail.class)
    @Column(name = "ID", updatable = false, nullable = false)
    private String uuid;

    @JsonView(JSONViews.Detail.class)
    @Column(name = "EMAIL", nullable = false, unique = true)
    private String email; // login

    @JsonView(JSONViews.Detail.class)
    @Column(name = "PASSWORD", nullable = false)
    private String password;

    @JsonView(JSONViews.Detail.class)
    @Column(name = "TEMP_PASS", nullable = false)
    private boolean temporalPassword;

    @JsonView(JSONViews.Simple.class)
    @Column(name = "NAME", nullable = false)
    private String name;

    @JsonView(JSONViews.Detail.class)
    @Column(name = "DATE_CREATED", updatable = false, nullable = false)
    private Date dateCreated;

    @Enumerated(value = EnumType.STRING)
    @JsonView(JSONViews.Detail.class)
    @Column(name = "ROLE", nullable = false)
    private UserRole userRole;

    @OneToMany(cascade = CascadeType.REMOVE, fetch = FetchType.LAZY, mappedBy = "author")
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonView(JSONViews.Detail.class)
    @Column(name = "ADVERTISEMENTS")
    private Set<Advertisement> advertisements;

    public User() {
    }

    public User(String email, String password, boolean temporalPassword, String name, Date dateCreated, UserRole userRole, Set<Advertisement> advertisements) {
        this.email = email;
        this.password = password;
        this.temporalPassword = temporalPassword;
        this.name = name;
        this.dateCreated = dateCreated;
        this.userRole = userRole;
        this.advertisements = advertisements;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isTemporalPassword() {
        return temporalPassword;
    }

    public void setTemporalPassword(boolean temporalPassword) {
        this.temporalPassword = temporalPassword;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public UserRole getUserRole() {
        return userRole;
    }

    public void setUserRole(UserRole userRole) {
        this.userRole = userRole;
    }

    public Set<Advertisement> getAdvertisements() {
        return advertisements;
    }

    public void setAdvertisements(Set<Advertisement> advertisements) {
        this.advertisements = advertisements;
    }
}
