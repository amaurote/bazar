package com.amaurote.bazar.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

@Entity
public class Notification {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID", updatable = false, nullable = false)
    private Long id;

    @Column(name = "EMAIL", updatable = false, nullable = false)
    private String email;

    @Column(name = "DATE_CREATED", updatable = false, nullable = false)
    private Date dateCreated;

    @Column(name = "DATE_EXPIRATION")
    private Date dateExpiration;

    @Column(name = "DATE_SENT")
    private Date dateSent;

    @Column(name = "MESSAGE", columnDefinition = "TEXT", updatable = false, nullable = false)
    private String message;

    public Notification() {
    }

    public Notification(Long id, String email, Date dateCreated, Date dateExpiration, Date dateSent, String message) {
        this.id = id;
        this.email = email;
        this.dateCreated = dateCreated;
        this.dateExpiration = dateExpiration;
        this.dateSent = dateSent;
        this.message = message;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getDateExpiration() {
        return dateExpiration;
    }

    public void setDateExpiration(Date dateExpiration) {
        this.dateExpiration = dateExpiration;
    }

    public Date getDateSent() {
        return dateSent;
    }

    public void setDateSent(Date dateSent) {
        this.dateSent = dateSent;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
