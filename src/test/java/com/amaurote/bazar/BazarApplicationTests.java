package com.amaurote.bazar;

import com.amaurote.bazar.service.AdvertisementService;
import com.amaurote.bazar.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BazarApplicationTests {

    private final UserService userService;
    private final AdvertisementService advertisementService;

    public BazarApplicationTests(UserService userService, AdvertisementService advertisementService) {
        this.userService = userService;
        this.advertisementService = advertisementService;
    }

    @Before
    public void initDb() {

    }

    @Test
    public void contextLoads() {
    }

}
