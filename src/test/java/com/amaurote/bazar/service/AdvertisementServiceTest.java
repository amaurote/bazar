package com.amaurote.bazar.service;

import com.amaurote.bazar.domain.AdvType;
import com.amaurote.bazar.domain.Advertisement;
import com.amaurote.bazar.domain.User;
import com.amaurote.bazar.domain.UserRole;
import com.amaurote.bazar.dto.AdvertisementDTO;
import com.amaurote.bazar.repository.AdvertisementRepository;
import com.amaurote.bazar.repository.UserRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class AdvertisementServiceTest {

    @Autowired
    private AdvertisementService advertisementService;

    @Autowired
    private AdvertisementRepository advertisementRepository;

    @Autowired
    private UserRepository userRepository;

    public AdvertisementServiceTest() {

    }

    @Before
    public void dbSeed() {
        // users
        User adminUser = new User("admin@admin.com", "$2a$10$wAGMl8F5tf8qZLo3X7/Iz.B7jRiFZFeTc0YLNlEIb8bEUAmcmCevW", false,"Admin User", new Date(), UserRole.ADMIN, new HashSet<>());
        User regularUser = new User("regular1@user.com", "$2a$10$wAGMl8F5tf8qZLo3X7/Iz.B7jRiFZFeTc0YLNlEIb8bEUAmcmCevW", false,"Regular User", new Date(), UserRole.USER, new HashSet<>());

        userRepository.saveAll(Arrays.asList(adminUser, regularUser));

        // adverts
        Advertisement advert1 = new Advertisement(regularUser, "Advertisement 1", "Advertisement 1 description.", AdvType.SELL, "150 $", new Date());
        Advertisement advert2 = new Advertisement(regularUser, "Advertisement 2", "Advertisement 2 description.", AdvType.SELL, "100 €", new Date());

        advertisementRepository.saveAll(Arrays.asList(advert1, advert2));
    }

    @Test
    public void getAdvertisementsById() {
        List<Advertisement> advertisements = (List<Advertisement>) advertisementRepository.findAll();
        Advertisement advertisement = advertisements.get(0);
        AdvertisementDTO dto = advertisementService.getAdvertisementsById(advertisement.getId());

        Assert.assertEquals("Items do not match.", advertisement.getTitle(), dto.getTitle());
    }

    @Test
    public void getAllAdvertisementsByType() {
        HashSet<AdvertisementDTO> buys = advertisementService.getAllAdvertisementsByType(AdvType.BUY);
        HashSet<AdvertisementDTO> sells = advertisementService.getAllAdvertisementsByType(AdvType.SELL);

        Assert.assertEquals("Unexpected items count.", buys.size(), 0);
        Assert.assertEquals("Unexpected items count.", sells.size(), 2);
    }

    @Test
    public void getAllAdvertisements() {
        HashSet<AdvertisementDTO> dtos = advertisementService.getAllAdvertisements();
        HashSet<Advertisement> advertisements = (HashSet<Advertisement>) advertisementRepository.findAll();

        Assert.assertTrue("Items count should be more than 0.", dtos.size() > 0 && advertisements.size() > 0);
        Assert.assertEquals("Service and repository results do not match.", dtos.size(), advertisements.size());
    }

    @Test
    public void getByTitle() {

    }

    @Test
    public void createNewAdvertisement() {
    }

    @Test
    public void deleteAdvertisementById() {
    }

    @Test
    public void sendResponseNotification() {
    }
}